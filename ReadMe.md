# SXMO ToDo

> A todo.txt script for SXMO

This script lets you view, edit, and create todos in the todo.txt format,

## Screenshots

![Main todo list](/screenshots/ToDo-list.png?raw=true)

This is the main todo list. It will show the ten highest priority todos. If there are more todos in the file, the rest can be found by selecting "Show all ToDos".

![Done todos](/screenshots/Done-menu.png?raw=true)

The completed todos are shown in this list. "Archive done" will move all completed todos to the done todo list.

![Action menu](/screenshots/Action-menu.png?raw=true)

This is the action menu that lets you mark as done, edit, or delete a todo.

## Configuration

The following configuration variables can be edited

`TODOTXT` - The todo.txt file to open and edit

`DONETXT` - The todo.txt file to archive completed todos to

`USEDATE` - If `1`, completion dates will be added when marking todos as done and removed when marking as not done

## Installing

1. Download `todo_txt.sh` 
2. Make any configuration changes
3. Mark as executable with `chmod +x todo_txt.sh`
4. Move it to `$XDG_CONFIG_HOME/sxmo/userscripts/`

The script can be run by opening the appmenu (`sxmo_appmenu.sh`) and selecting Scripts > todo\_txt.sh
