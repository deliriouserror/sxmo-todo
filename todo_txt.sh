#!/bin/sh
# title="$icon_chk ToDo List"

# todo.txt script for SXMO
# https://gitlab.com/deliriouserror/sxmo-todo

# MIT License
#
# Copyright (c) 2022 Deliriouserror
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# shellcheck source=configs/default_hooks/sxmo_hook_icons.sh
if command -v sxmo_hook_icons.sh &> /dev/null
then
	. sxmo_hook_icons.sh
else
	. sxmo_icons.sh
fi

# shellcheck source=scripts/core/sxmo_common.sh
. sxmo_common.sh

# -----------------
# | Configuration |
# -----------------
#
# TODOTXT - File path to the todo.txt that will be opened
# DONETXT - File path to the todo.txt that archived todos will be saved to
# USEDATE - 0 or 1. If 1, completion dates will be added when marking todos as done and removed when marking as not done

TODOTXT="/home/user/Documents/Todo/todo.txt"
DONETXT="/home/user/Documents/Todo/todo_done.txt"
USEDATE=1

todoselect() {
# Set up menu options
	NAME="ToDo"
	TODOLIST="$(cat $TODOTXT | sed '/^[[:blank:]]*$/d' | sort -n)"
	TODOTODO="$(echo "$TODOLIST" | grep -v "x ")"
	TODODONE="$(echo "$TODOLIST" | grep "x " | cut -c 3- | sort -n | sed -e 's/^/x /')"
	OPTIONS="$(echo "$TODOTODO" | head)"

	[ $(echo "$TODOTODO" | wc -l) -gt 10 ] && OPTIONS="$OPTIONS
$icon_lst Show all ToDos"
	echo "$TODODONE" | grep -q "x " && OPTIONS="$OPTIONS
$icon_lst Show done"

	OPTIONS="$OPTIONS
$icon_pls New ToDo
$icon_cls Close menu"

# Select a menu item to act on
	ITEM="$(echo "$OPTIONS" | sxmo_dmenu.sh -p $NAME)"

	case "$ITEM" in
		"$icon_lst Show all ToDos" )
			NAME="All"
			OPTIONS="$TODOTODO
$icon_pls New ToDo
$icon_ret Go back
$icon_cls Close menu"
			ITEM="$(echo "$OPTIONS" | sxmo_dmenu.sh -p $NAME)"
			;;

		"$icon_lst Show done" )
			NAME="Done"
			OPTIONS="$TODODONE
$icon_dir Archive done
$icon_ret Go back
$icon_cls Close menu"
			ITEM="$(echo "$OPTIONS"| sxmo_dmenu.sh -p $NAME)"
			;;
	esac

	case "$ITEM" in
		"$icon_ret Go back" )
			todoselect
			;;

		"$icon_pls New ToDo" )
			todocreate
			;;

		"$icon_dir Archive done" )
			todoarchive
			todoselect
			;;

		"$icon_cls Close menu" )
			exit 0
			;;
		* )
			echo "$OPTIONS" | grep -q "$ITEM" && actionselect
			;;
	esac
	todoselect
}

actionselect() {
# Choose an action for the todo
	NAME="Action"
	TODO=$ITEM
	OPTIONS="$TODO
$icon_chk Toggle done
$icon_edt Edit ToDo
$icon_del Delete ToDo
$icon_ret Go back
$icon_cls Close menu"
	ACTION="$(echo "$OPTIONS" | sxmo_dmenu.sh -p $NAME)"

	case "$ACTION" in
		"$TODO" )
			actionselect
			;;

		"$icon_chk Toggle done" )
			tododone
			;;

		"$icon_edt Edit ToDo" )
			todoedit
			;;

		"$icon_del Delete ToDo" )
			tododelete
			;;

		"$icon_ret Go back" )
			todoselect
			;;

		"$icon_cls Close menu" )
			exit 0
			;;
	esac

	todoselect
}

todocreate() {
	NAME="Create"
	sxmo_keyboard.sh open
	TEMPFILE="$(dirname $TODOTXT)/newtodo-temp.txt"

	sxmo_terminal.sh "$EDITOR" "$TEMPFILE"
	NEWTODO=$(head -n 1 "$TEMPFILE")
	echo $NEWTODO >> $TODOTXT
	rm "$TEMPFILE"

	ITEM=$NEWTODO
	actionselect
}

todoedit() {
	NAME="Edit"
	sxmo_keyboard.sh open
	TEMPFILE="$(dirname $TODOTXT)/edittodo-temp.txt"
	echo "$TODO" > "$TEMPFILE"

	sxmo_terminal.sh "$EDITOR" "$TEMPFILE"
	EDITTODO=$(head -n 1 "$TEMPFILE")
	sed -i "s/$TODO/$EDITTODO/" $TODOTXT
	rm "$TEMPFILE"

	ITEM=$EDITTODO
	actionselect
}

tododone() {
	NAME="Toggle"
	if [ "$(echo $TODO | cut -c 1,2)" = "x " ]; then
		# If date present, remove date
		if [ $USEDATE -eq 1 ] && [ "$(echo "$TODO" | grep "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]")" ] ; then
			NEWTODO=$(echo "$TODO" | sed 's/^x [0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\} //g')
			sed -i "s/$TODO/$NEWTODO/" $TODOTXT
			TODO=$NEWTODO
		else
			sed -i "s/^$TODO$/$(echo $TODO | cut -c 3-)/" $TODOTXT
		fi

		# If priority tag present, remove tag and add priority back
		if echo "$TODO" | grep -q 'pri:[A-Z]' ; then
			PRI=$(echo "$TODO" | grep -o 'pri:[A-Z]' | cut -c 5)
			NEWTODO=$(echo "$TODO" | sed 's/ pri:[A-Z]//' | sed "s/^/($PRI) /")
			sed -i "s/$TODO/$NEWTODO/" $TODOTXT
		fi
		echo "'Not done: $TODO'" | sxmo_dmenu.sh -p $NAME > /dev/null
	else
		# If priority present, remove it and add tag
		if echo "$TODO" | grep -q '([A-Z])' ; then
			PRI=$(echo "$TODO" | grep -o '([A-Z])' | cut -c 2)
			NEWTODO="$(echo "$TODO" | sed "s/^($PRI) //") pri:$PRI"
			sed -i "s/$TODO/$NEWTODO/" $TODOTXT
			TODO=$NEWTODO
		fi
		DATE="$(date +%Y-%m-%d)"
		if [ $USEDATE -eq 1 ]; then
			sed -i "s/^$TODO$/x $DATE $TODO/" $TODOTXT
		else
			sed -i "s/^$TODO$/x $TODO/" $TODOTXT
		fi
		echo "'Done: $TODO'" | sxmo_dmenu.sh -p $NAME > /dev/null
	fi
}

tododelete() {
	NAME="Delete"
	sed -i "/^$TODO$/d" $TODOTXT
	echo "$icon_del Deleted: '$TODO'" | sxmo_dmenu.sh -p $NAME > /dev/null
}

todoarchive() {
	NAME="Archive"
	echo "$TODODONE" >> $DONETXT
	echo "$TODOTODO" > $TODOTXT
	echo "Done ToDos archived" | sxmo_dmenu.sh -p $NAME > /dev/null
}

todoselect
